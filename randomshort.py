#!/usr/bin/env python3


import socket
import shelve
import random
import string
import urllib.request

PORT = 8080
HOSTNAME = ""

FORM = """
    <hr>
        <form action="/" method="post">
            <div>
            <label>Url a acortar: </label>
                <input type ="text" name="url" required>
            </div>
            <div>
                <input type ="submit" value="Enviar">
            </div>
        </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
        {form}
    </div>
    <div>
        <p>Urls acortadas:</p>
        <ul> 
        {urls}
        </ul>
    </div>
  </body>
</html>
"""


PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
    <body>
    <p>Unprocessable POST: {body}.</p>
    </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
  <p>Recurso no disponible: {recurso}</p>
  </body>
</html>
"""


URLs = shelve.open("urls.db")

class webApp:

    def parse_info(self, request):

        parse_request = request.split()
        method = parse_request[0] #Obtengo el metodo
        print(f"{method} 1 ")
        resource = parse_request[1] #Obtengo el recurso
        print(f"{resource} 2")
        body_start = request.find("\r\n\r\n")
        if body_start == -1:
            body = None
        else:
            body = request[body_start + 4:] #Obtengo el cuerpo de la peticion
        print(f"{body} 3")

        return method, resource, body


    def do_method(self, method, resource, body):
        if method == "GET":
            #Hago metodo GET
            code, page = self.do_get(resource)
        elif method == "POST":
            #Hago metodo POST
            code, page = self.do_post(resource, body)
        else:
            #Si no es GET ni POST, le digo no tenemos el metodo
            code = "405 Method Not Allowed"
            page = PAGE_NOT_ALLOWED.format(method=method)
        return code, page

    #Metdodo que me permite obtener las urls almacenadas en la base de datos, y mostrarlas en la pagina web en ul, donde ul nos permite hacer una lista
    def get_urls(self):
        urls = ""
        for random_url, url in URLs.items():
            urls += f"<li><a href='{url}'>{random_url}</a></li>"
        return urls

    def do_get(self, resource):
        #Si el recurso es /, le envio la pagina principal, con las urls almacenadad
        if resource == "/":
            code = "200 OK"
            urls = self.get_urls()
            page = PAGE.format(form=FORM, urls=urls)
            return code, page
        else:
            #Si no es /, le redirecciono al recurso pedido
            short_code = resource[1:]  # Quitamos la barra inicial del recurso
            if short_code in URLs:
                real_url = URLs[short_code]
                code = f"302 Found\r\nLocation: {real_url}"
                page = None
            else:
                #en caso de no encontrar el recurso pedido, le digo que no existe
                code = "404 Not Found"
                page = PAGE_NOT_FOUND.format(recurso=resource)
            return code, page

    def do_post(self, resource, body):
        #Si el recurso es /, le envio la pagina principal, con las urls almacenadad
        if resource == "/":
            fields = urllib.parse.parse_qs(body)
            print("Fields:", fields)
            #Comprobramos que la url que se ha introducido es correcta
            if fields:
                if "url" in fields:
                    url = fields["url"][0] #Nos quemados con la url
                    # En caso de que la url no comience con http o https, se agrega http al principio
                    if not url.startswith("http://") and not url.startswith("https://"):
                        url = "https://" + url
                        #Comprobamos que la url no se encuentre ya en la base de datos
                    if url not in URLs.values():
                        random_url = ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))
                        URLs[random_url] = url
                        urls = self.get_urls()
                        code = "200 OK"
                        page = PAGE.format(form=FORM, urls=urls)
                    else:
                        #Si esta la url ya en la base de datos, le envio la pagina principal, con las urls almacenadas
                        code = "200 OK"
                        urls = self.get_urls()
                        page = PAGE.format(form=FORM, urls=urls)
                else:
                    code = "422 Bad Request"
                    page = PAGE_UNPROCESSABLE.format(body=body)
            else:
                code = "400 Bad Request"
                page = PAGE_UNPROCESSABLE.format(body=body)
        else:
            code = "405 Method Not Allowed"
            page = PAGE_NOT_ALLOWED.format(method="POST")
        return code, page



    def __init__(self, hostname, port):

        # Create a TCP objet socket and bind it to a port
        MyServer = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Creo el socket, TCP de la familia de direcciones IPv4
        MyServer.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #Pongo el socket en modo reusar, para poder reusar el puerto
        MyServer.bind((hostname, port)) #Bindeo el socket a la direccion y puerto que me indiquen
        MyServer.listen(5) #Escucho hasta 5 conexiones simultaneas

        try:
            print("Waiting for connections")
            while True:
                (revSocket, address) = MyServer.accept() #Acepto la conexion
                print(f"Connection from {address}")
                received = revSocket.recv(2048).decode() #Recibo la peticion
                method, resource, body = self.parse_info(received) #Parseo la peticion
                code, page = self.do_method(method, resource, body) #Proceso la peticion
                response_code = "HTTP/1.1 " + code + "\r\n\r\n"
                response_page = page
                r = '\r\n'
                revSocket.send(response_code.encode()) #Envio la respuesta
                if page:
                    revSocket.send(response_page.encode()) #Envio la respuesta
                    revSocket.send(r.encode())
                revSocket.close()

        except KeyboardInterrupt:
            print("Closing server")
            MyServer.close()
        except ConnectionError:
            print("Problem with connection")
            MyServer.close()

if __name__ == "__main__":
    App = webApp(HOSTNAME, PORT)